class Todo {
  final num id;
  final String title;
  final bool completed;

  Todo({ this.id, this.title, this.completed });

  factory Todo.fromJson(Map json) {
    return Todo(
      id: json['id'],
      title: json['title'],
      completed: json['completed'],
    );
  }
}