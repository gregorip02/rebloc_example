import 'package:rebloc/rebloc.dart';
import 'package:flutter/material.dart';
import 'package:rebloc_example/state/state.dart';
import 'package:rebloc_example/models/todo.dart';

void main(List<String> args) {
  final Store store = Store<AppState>(
    initialState: AppState.initialState(),
    blocs: [
      TodoBloc()
    ]
  );

  runApp(StoreProvider<AppState>(
    store: store,
    child: FirstBuildDispatcher<AppState>(
      action: FetchingTodosAction(),
      child: App()
    )
  ));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.green
      ),
      title: 'Rebloc example',
      home: AppHome()
    );
  }
}

class AppHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelSubscriber<AppState, String>(
      converter: (AppState state) => state.networkStatus,
      builder: (BuildContext context, DispatchFunction dispatch, String status) {
        return Scaffold(
          body: status != 'completed' ? NetworkStatus(status) : MyTodosList(),
          floatingActionButton: MyFloatingActionButtom(),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat
        );
      }
    );
  }
}

class NetworkStatus extends StatelessWidget {
  final String status;

  final TextStyle errorStyle = TextStyle(
    color: Colors.red,
    fontSize: 16.0
  );

  NetworkStatus(this.status);

  @override
  Widget build(BuildContext context) {
    if (status == 'loading' || status == null) {
      return Center(
        child: CircularProgressIndicator()
      );
    }
    if (status == 'erred') {
      return Center(
        child: Text('Error de conexión', style: errorStyle)
      );
    }

    return Center();
  }
}


class MyTodosList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelSubscriber<AppState, List<Todo>>(
      converter: (AppState state) => state.todos,
      builder: (BuildContext context, DispatchFunction dispatch, List<Todo> todos) {
        return ListView.builder(
          itemCount: todos.length,
          itemBuilder: (BuildContext context, int index) {
            final Todo todo = todos[index];

            return ListTile(
              leading: Icon(
                todo.completed ? Icons.check_circle : Icons.crop_square,
                color: todo.completed ? Colors.green : Colors.red
              ),
              title: Text(todo.title)
            );
          },
        );
      }
    );
  }
}


class MyFloatingActionButtom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelSubscriber<AppState, bool>(
      converter: (AppState state) => state.networkStatus == 'loading',
      builder: (BuildContext context, DispatchFunction dispatch, bool loading) {
        return FloatingActionButton(
          child: Icon(Icons.refresh),
          onPressed: () => loading ? null : dispatch(FetchingTodosAction())
        );
      }
    );
  }
}

