import 'dart:async';
import 'package:rebloc/rebloc.dart';
import 'package:rebloc_example/network.dart';
import 'package:rebloc_example/state/state.dart';
import 'package:rebloc_example/state/actions.dart';

class TodoBloc extends SimpleBloc<AppState> {
  // Los reductores especifican cómo cambia el estado de la aplicación en respuesta
  // a las acciones enviadas al Store. Recuerde que las acciones solo describen
  // lo que sucedió, pero no describen cómo cambia el estado de la aplicación.
  //
  // See https://redux.js.org/basics/reducers#reducers
  @override
  AppState reducer(AppState state, Action action) {
    if (action is FetchingTodosAction) {
      return state.copyWith(networkStatus: 'loading');
    }
    else if (action is FetchedTodosAction) {
      return state.copyWith(todos: action.todos, networkStatus: 'completed');
    }
    else if (action is ErrorFetchingTodosAction) {
      return state.copyWith(networkStatus: 'erred');
    }

    return state;
  }

  @override
  FutureOr<Action> afterware(DispatchFunction dispatch, AppState state, Action action) {
    if (action is FetchingTodosAction) {
      fetchTodos().then((todos) => dispatch(FetchedTodosAction(todos))).catchError((e) =>
        dispatch(ErrorFetchingTodosAction()));
    }

    return action;
  }
}
