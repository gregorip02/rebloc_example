import 'package:rebloc/rebloc.dart';
import 'package:rebloc_example/models/todo.dart';

// Actions
//
// Las acciones son cargas de información que envían datos desde su aplicación a
// su Store. Son la única fuente de información para el Store.
//
// See https://redux.js.org/basics/actions#actions
class FetchingTodosAction extends Action {}

class FetchedTodosAction extends Action {
  final List<Todo> todos;
  FetchedTodosAction(this.todos);
}

class ErrorFetchingTodosAction extends Action {}
