export 'package:rebloc_example/state/blocs.dart';
export 'package:rebloc_example/state/actions.dart';

import 'package:rebloc_example/models/todo.dart';

class AppState {
  final List<Todo> todos;

  // networkStatus almacena el estado de una peticion
  // http, inicialmente tiene un valor null que representa ausencia
  // de peticiones, sus valores pueden ser:
  //
  // null: inactivo
  // loading: cargando recursos
  // completed: carga completa
  // erred: carga errada
  final String networkStatus;

  AppState(this.todos, { this.networkStatus });

  factory AppState.initialState() => AppState([]);

  AppState copyWith({ List<Todo> todos, String networkStatus }) {
    return AppState(todos, networkStatus: networkStatus);
  }
}
