import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rebloc_example/models/todo.dart';

Future <List<Todo>> fetchTodos() async {
  print('Conectando con jsonplaceholder...');

  final response = await http.get('https://jsonplaceholder.typicode.com/todos')
    .timeout(Duration(seconds: 10), onTimeout: () {
      print('Tiempo agotado en la petición');
      return throw 'Error timeout';
    });

  if (response.statusCode == 200) {
    print('Conexión terminada');
    return List.from(json.decode(response.body)).map((todo) =>
      Todo.fromJson(Map.from(todo))).toList();
  }

  return throw 'Error';
}